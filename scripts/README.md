# Scripts

This directory contain all the scripts for the project.

Use sample script as follows:

```
######################################
# Copyright License
# Authors name
# Description of script

######################################

Class and functions goes here.

```

Note:
 - Describe detailed documetation as docstring for all the classes and functions.
 - Use PEP8 conventions for scripts.
 - Make sure your code is properly formatted. (Use
   [black](https://pypi.org/project/black/) as code formatter)
